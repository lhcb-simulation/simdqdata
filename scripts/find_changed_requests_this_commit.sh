#!/bin/bash
set -euo pipefail

git diff --name-only HEAD~1 | grep "${1}$"
