#!/bin/bash
set -euo pipefail

INPUT_FILE=$1 # list of missing URLs, one per line

while read -r url
do
    grep -Flx "PFN: $url" */*/*.yml
done < $INPUT_FILE

