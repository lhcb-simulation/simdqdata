#!/usr/bin/env python

import yaml
import re
from copy import copy
from pathlib import Path

from DIRAC.Core.Base import Script
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from LHCbDIRAC.TransformationSystem.Client.TransformationClient import (
    TransformationClient,
)
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
    ProductionRequestClient,
)
from DIRAC.DataManagementSystem.Client.DataManager import DataManager

Script.parseCommandLine()

bc = BookkeepingClient()
tc = TransformationClient()
pc = ProductionRequestClient()
dm = DataManager()

REQ_KEYS = {
    "RequestID",
    # 'ParentID',
    # 'MasterID',
    # 'RequestAuthor',
    "RequestName",
    # 'RequestType',
    # 'RequestState',
    # 'RequestPriority',
    # 'RequestPDG',
    "RequestWG",
    "SimCondition",
    # 'SimCondID',
    "SimCondDetail",
    "ProPath",
    # 'ProID',
    # 'ProDetail',
    # 'EventType',
    # 'NumberOfEvents',
    # 'Description',
    # 'Comments',
    # 'Inform',
    # 'RealNumberOfEvents',
    # 'IsModel',
    "Extra",
    "RetentionRate",
    "FastSimulationType",
    # 'StartingDate',
    # 'FinalizationDate',
    # 'HasSubrequest',
    # 'bk',
    # 'bkSrTotal',
    # 'bkTotal',
    # 'rqTotal',
    # 'crTime',
    # 'upTime'
}

TFM_KEYS = {
    "TransformationID",
    # 'TransformationName',
    # 'Description',
    # 'LongDescription',
    # 'CreationDate',
    # 'LastUpdate',
    # 'AuthorDN',
    # 'AuthorGroup',
    "Type",
    # 'Plugin',
    # 'AgentType',
    # 'Status',
    # 'FileMask',
    # 'TransformationGroup',
    # 'GroupSize',
    # 'InheritedFrom',
    # 'Body',
    # 'MaxNumberOfTasks',
    # 'EventsPerTask',
    # 'TransformationFamily',
    # 'Hot',
}

assert REQ_KEYS.isdisjoint(TFM_KEYS)


def get_lfns(prod_id: int, filetype: str) -> list[str]:
    result = bc.getProductionFiles(prod_id, filetype)

    if not result["OK"]:
        raise RuntimeError(result["Message"])
    return list(result["Value"].keys())[:1]


def get_lfns_with_pfns(lfns: list[str]) -> list[str]:
    result = dm.getReplicas(lfns, preferDisk=True)

    if not result["OK"]:
        raise RuntimeError(result["Message"])

    replicas = result["Value"]["Successful"]
    failed_replicas = result["Value"]["Failed"]
    pfns = [
        list(replicas[lfn].values())[0] for lfn in lfns if lfn not in failed_replicas
    ]
    pfns = [re.sub(r"^(\w+://)[^/]+@", r"\1", url) for url in pfns]

    return list(replicas.keys()), pfns


def get_filetypes(prod_id: int) -> list:
    result = bc.getProductionOutputFileTypes({"Production": prod_id})

    if not result["OK"]:
        raise RuntimeError(result["Message"])
    return list(result["Value"].keys())


def get_transformations(req_id: int) -> dict:
    result = tc.getTransformations({"TransformationFamily": req_id})

    if not result["OK"]:
        raise RuntimeError(result["Message"])

    transformations = result["Value"]
    return transformations


def get_requests(
    subrequestFor: int,
    sortBy: str = "RequestID",
    sortOrder: str = "DESC",
    offset: int = 0,
    limit: int = 100,
    rFilter: dict = {"RequestType": "Simulation"},
) -> dict:
    result = pc.getProductionRequestList(
        subrequestFor, sortBy, sortOrder, offset, limit, rFilter
    )

    if not result["OK"]:
        raise RuntimeError(result["Message"])

    requests = result["Value"]["Rows"]
    return [r for r in requests if r["RequestState"] in {"Done", "Accepted", "Active"}]


def is_dict_string(s: str) -> bool:
    if type(s) is not str:
        return False
    try:
        return type(yaml.safe_load(s)) is dict
    except yaml.parser.ParserError:
        return False


def parse_dict_strings(d: dict) -> dict:
    return {
        key: yaml.safe_load(value) if is_dict_string(value) else value
        for key, value in d.items()
    }


def format_request_metadata(request: dict) -> dict:
    req_dict = {key: request[key] for key in REQ_KEYS}
    req_dict = parse_dict_strings(req_dict)
    transformations = get_transformations(request["RequestID"])
    tfm_dicts = [
        {key: transformation[key] for key in TFM_KEYS}
        for transformation in transformations
    ]
    tfm_dicts = [parse_dict_strings(tfm_dict) for tfm_dict in tfm_dicts]

    metadata = {}
    for tfm_dict in tfm_dicts:
        prod_id = tfm_dict["TransformationID"]
        for filename in filter(lambda f: f.endswith("HIST"), get_filetypes(prod_id)):
            lfns = get_lfns(prod_id, filename)
            lfns, pfns = get_lfns_with_pfns(lfns)
            if len(pfns) == 0:
                continue
            step_index = int(lfns[0].split("/")[-1].split(".")[0].split("_")[-1])
            output_filename = f"{step_index}-{filename}.yml"
            metadata[output_filename] = copy(req_dict)
            metadata[output_filename].update(tfm_dict)
            metadata[output_filename].update(
                {
                    "LFN": lfns[0],
                    "PFN": pfns[0],
                }
            )

    return metadata


def get_all_inner_requests(outer_req_id):
    for request in get_requests(outer_req_id):
        if request["HasSubrequest"]:
            req_id = request["RequestID"]
            yield from get_all_inner_requests(req_id)
        else:
            yield request


# all_requests = list(get_all_inner_requests(0))

structured_requests = {}

all_outer_requests = get_requests(0)

for request in all_outer_requests[:100]:
    req_id = request["RequestID"]
    req_id_str = str(req_id).rjust(8, "0")
    if request["HasSubrequest"]:
        all_subrequests = list(get_all_inner_requests(req_id))
        if len(all_subrequests) == 0:
            continue
    else:
        all_subrequests = [request]
    structured_requests[req_id_str] = {}
    for subrequest in all_subrequests:
        event_type = subrequest["EventType"]
        metadata = format_request_metadata(subrequest)
        if len(metadata) == 0:
            print(f"Skipping {subrequest['RequestID']}")
            continue
        structured_requests[req_id_str][event_type] = metadata
    if len(structured_requests[req_id_str].keys()) == 0:
        del structured_requests[req_id_str]

for req_id, subrequest in structured_requests.items():
    req_dir = Path(req_id)
    for event_type, steps in subrequest.items():
        subreq_dir = req_dir / event_type
        subreq_dir.mkdir(parents=True, exist_ok=True)
        for filename, metadata in steps.items():
            with open(subreq_dir / filename, "w") as f:
                yaml.dump(metadata, f)
