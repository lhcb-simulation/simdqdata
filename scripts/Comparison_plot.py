import yaml
import ROOT


#function that takes path of decay file and plots all the histgrams and the ref histograms on top of each other

##### Base Class #####
class analyzer:

    # initialize class with it's name, histogram and reference_histogram to analyze for derived classes
    def __init__( self, name , histogram, reference_histogram, iteration, canvas, legends):

        self.name = name
        self.hist = histogram
        self.ref_hist = reference_histogram
        self.iteration = iteration
        self.canvas = canvas
        self.legends = legends
       

    # performs the analysis on the histogram
    def analyze():

        pass


##### Derived classes #####

 
#function to plot the histogram and its reference

class ComparisonPlot( analyzer ):
    
    def analyze( self ):

        self.hist.SetStats(0)
        self.ref_hist.SetStats(0)
        pad = self.canvas.cd(self.iteration + 1)  # Activate the i+1 pad
        pad.SetGrid()  # Optional: Add grid lines
        self.ref_hist.Draw("sames hist")
        self.hist.Draw("sames hist")
        self.hist.SetLineColor(2)
        
        KS = self.hist.KolmogorovTest(self.ref_hist)
        Chi2 = self.hist.Chi2Test(self.ref_hist)

        legend = self.legends[self.iteration]
        legend.SetTextSize(0.035)
        legend.AddEntry(self.hist, "hist".format(self.iteration + 1))
        legend.AddEntry(self.ref_hist, "ref hist".format(self.iteration + 1))
        legend.AddEntry(self.hist, f"Chi2: {Chi2:.3f}".format(self.iteration + 1), "")
        legend.AddEntry(self.hist, f"KS: {KS:.3f}".format(self.iteration + 1), "")
        legend.Draw("sames hist")
        pad.Update()    
        


            
