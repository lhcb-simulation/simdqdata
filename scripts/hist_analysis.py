import yaml
import ROOT
import math
import argparse
import os
import importlib
from pathlib import Path
from junit_xml import TestSuite, TestCase
import time


# Defining some functions
def openYml(yml):
    with open(yml, "r") as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data

def tokenify(url: str) -> str:
    if token := os.environ.get("EOS_TOKEN"):
        return f"{url}?xrd.wantprot=unix&authz=zteos64:{token}%3d%3d"
    return url


########################################## Open ymls #############################################

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ymlFile", help = "path to YAML file containing the request metadata and PFNs of histogram files (e.g. '00105892/13104007/1-GAUSSHIST.yml')")
    parser.add_argument("AnalysisFile", help = "name of yml file containing parameters of the checks")
    parser.add_argument("--use-token", action = "store_true", help = "Authenticate with an EOS token stored in the environment variable EOS_TOKEN")
    parser.add_argument("--plot-dir", default="Comparison_plots", help = "Authenticate with an EOS token stored in the environment variable EOS_TOKEN")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)

    metadata = openYml(args.ymlFile)
    url = metadata["PFN"]
    ref_url = metadata["refPFN"]
    if args.use_token:
        url = tokenify(url)
        ref_url = tokenify(ref_url)

    #extract decay file number as string
    parts = args.ymlFile.split('/')
    DecNumber = '_'.join(parts[-3:-1]).replace('/', '_')

    #load histogram and ref histogram files
    hist_file, ref_hist_file = ROOT.TFile.Open(url), ROOT.TFile.Open(ref_url)

    #open yml file containing parameters for analysis
    sim_analysis = openYml(args.AnalysisFile)

    #set the output directory for plots and create if doesn't exist
    plot_dir = Path(args.plot_dir) / DecNumber
    plot_dir.mkdir(parents=True, exist_ok=True)


################################################ checks ###################################################

    #output is saved to log file

    # check whether means of primary vertex location (x, y, z) lie within a tolerance around the expectation value of the reference production
    # check whether StdDev of primary vertex location (x, y, z) lie within a tolerance around the expectation value of the reference production
    # plot request and its reference and calculate KS and Chi2 value

    from Comparison_plot import ComparisonPlot

    for analysis_name, analysis_params in sim_analysis.items():
        module = importlib.import_module(f'AutomaticAnalyses.algorithms.{analysis_params["file"]}')
        check_class = getattr(module, analysis_params["class"])

        #plot histogram and its reference
        n_hists = len(analysis_params["hist_name"])
        n_x = min(6, n_hists)
        n_y = math.ceil(n_hists/n_x)
        c = ROOT.TCanvas("c", "Comparison histograms", 600*n_x, 400*n_y)
        c.Divide(n_x,n_y)
        legends = [ROOT.TLegend(0.6, 0.75, 0.9, 0.9) for _ in range(n_hists)]

        # set up unit test report
        test_cases = []

        for i, hist_name in enumerate(analysis_params["hist_name"]):
            hist, ref_hist = hist_file.Get(hist_name), ref_hist_file.Get(hist_name)
            start = time.time()
            check = check_class(analysis_name, hist, ref_hist)
            check.analyze()
            end = time.time()
            plot = ComparisonPlot(analysis_name, hist, ref_hist, i, c, legends)
            plot.analyze()
            test_cases.append(TestCase(hist.GetTitle(), f"{analysis_name}[{DecNumber}]", end - start, check.info_message, check.error_message))

        ts = TestSuite(analysis_name, test_cases)
        with open(plot_dir / f"{analysis_name.replace(' ', '_')}.xml", "w") as f:
            TestSuite.to_file(f, [ts], prettyprint=False)

        c.Update()
        c.SaveAs(str(plot_dir / f"{analysis_name}.png"))


    hist_file.Close()
    ref_hist_file.Close()

if __name__ == '__main__':
    main()


