#!/usr/bin/env python

from glob import glob
import yaml
import sys

def main(max_paths = -1):
    yaml_files = glob("*/*/*HIST.yml")
    report = {True: [], False: []}
    for path in yaml_files:
        with open(path, "r") as f:
            metadata = yaml.safe_load(f)
        has_ref = metadata["LFN"] != metadata["refLFN"]
        report[has_ref] += [path]
    max_paths = min(len(report[True]), max_paths)
    print("\n".join(report[True][:max_paths]))


if __name__ == "__main__":
    max_paths = -1
    if (nargs := len(sys.argv)) == 2:
        max_paths = int(sys.argv[1])
    elif nargs >= 2:
        print(f"too many arguments, expect 1 or 0 ({nargs-1} given)", file=sys.stderr)
        exit(1)
    main(max_paths)
