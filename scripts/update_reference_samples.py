#!/usr/bin/env python

from glob import glob
import re
import yaml
from collections import defaultdict

REFERENCES = {}

def deduce_key(path: str, metadata: dict) -> str:
    sim_version = metadata["ProPath"].split("/")[0]
    request_id, event_type, filename = path.split("/")
    step_index, filetype = filename.removesuffix(".yml").split("-")
    sim_cond = metadata["SimCondition"]
    # Assumption: the plots are minor-version-independent
    try:
        major_sim_version = re.search(r"(Sim|Gen)\d\d", sim_version)[0]
    except:
        major_sim_version = sim_version
    # Assumption: event type starting with X can be compared to X0000000
    ref_event_type = event_type[0]+"0"*7
    # Assumption: any generator that uses Pythia8 can be compared to plain Pythia8
    # e.g. BcVegPyPythia8 -> Pythia8
    if "Pythia8" in (segments := sim_cond.split("-"))[-1]:
        ref_sim_cond = "-".join(segments[:-1]+["Pythia8"])
    else:
        ref_sim_cond = sim_cond
    key = "/".join([
        major_sim_version,
        ref_sim_cond,
        ref_event_type,
        filetype,
    ])
    return key

def find_references() -> dict[str, dict[str, str]]:
    yaml_files = glob("*/?0000000/*.yml")
    samples = {}
    for path in yaml_files:
        with open(path, "r") as f:
            metadata = yaml.safe_load(f)
        key = deduce_key(path, metadata)
        # Add samples
        if key in samples:
            print(f"Already have a sample for {key}")
            continue
        samples.update({
            key: {
                f"ref{key}": metadata[key] for key in ["PFN", "LFN"]
            }
        })
    return samples

def deduce_reference(path: str, metadata: dict) -> tuple[bool, dict[str, str]]:
    request_id = metadata["RequestID"]
    try:
        key = deduce_key(path, metadata)
        ref = REFERENCES[key]
        return True, ref
    except KeyError:
        return False, {
            # By default use self as reference
            f"ref{key}": metadata[key] for key in ["PFN", "LFN"]
        }


def main():
    yaml_files = glob("*/*/*HIST.yml")
    report = {True: [], False: []}
    unref_keys = defaultdict(int)
    for path in yaml_files:
        with open(path, "r") as f:
            metadata = yaml.safe_load(f)
        status, ref = deduce_reference(path, metadata)
        metadata.update(ref)
        report[status] += [path]
        if not status:
            unref_keys[deduce_key(path, metadata)] += 1
        with open(path, "w") as f:
            yaml.dump(metadata, f)
    report_file = "missing_refs.tsv"
    with open(report_file, "w") as f:
        for key in sorted(unref_keys):
            f.write(f"{unref_keys[key]}\t{key}\n")
    print(f"Found reference samples for {len(report[True])} files.")
    print(f"Failed for {len(report[False])} files. See {report_file} for list of missing reference samples.")


if __name__ == "__main__":
    REFERENCES = find_references()
    main()
