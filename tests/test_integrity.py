###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
import os
from XRootD import client
from XRootD.client.flags import OpenFlags
import yaml
from typing import Iterable
from pathlib import Path

def tokenify(url: str) -> str:
    token = os.environ["EOS_TOKEN"]
    return f"{url}?xrd.wantprot=unix&authz={token}"


def get_pfns() -> Iterable[str]:
    yaml_files = Path(__file__).parent.parent.glob("**/*HIST.yml")
    for fname in yaml_files:
        with open(fname, "r") as f:
            yield yaml.safe_load(f).get("PFN")


@pytest.mark.parametrize(
    "url", get_pfns()
)
def test_PFN_exists(url: str) -> bool:
    with client.File() as f:
        status, _ = f.open(tokenify(url), OpenFlags.READ)
        f.close()
    assert status.ok


if __name__=="__main__":
    missing_urls = []
    for url in get_pfns():
        try:
            test_PFN_exists(url)
        except AssertionError:
            missing_urls += [url]
    print("\n".join(missing_urls))
